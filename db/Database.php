<?php

namespace db;

use PDO;

class Database {

    private $dsn;
    private $dbname;
    private $host;
    private $charset;
    private $user;
    private $password;
    private $dbh;
    protected $data;
    protected $tableName;

    public static function instance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new Database();
        }
        return $inst;
    }

    protected function __construct() {
        $this->dsn = 'mysql:dbname=onepaar;host=127.0.0.1;charset=utf8';
        $this->user = 'root';
        $this->password = '';

        try {
            $this->dbh = new PDO($this->dsn, $this->user, $this->password);
//            echo 'Connected<br>';
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            exit;
        }
    }

    public function query($query) {
        return $this->dbh->query($query);
    }

    public function findOneByQuery($query, $key) {
        $res = $this->dbh->query($query);
        if ($res != FALSE) {
            foreach ($res as $value) {
                return($value[$key]);
            }
        }
        return "";
    }

    public function insert($data) {
        $queryHead = "INSERT INTO " . $this->getTableName();
        $query = "";
        $queryVal = " VALUES(";
        $queryCol = "(";


        foreach ($data as $key => $value) {

            $queryCol .= $key . ", ";
            $queryVal .= "'" . $value . "', ";

//            sout($value);
        }
//        sout($queryVal);
        $queryCol = substr($queryCol, 0, -2);
        $queryCol .= ")";
        $queryVal = substr($queryVal, 0, -2);
        $queryVal .= "), ";
        $queryValues = $queryVal;
        $queryValues = substr($queryValues, 0, -2);
        $query = $queryHead . $queryCol . $queryValues;
//        sout($query);
//            $query = $queryHead . $queryValues;
        $succes = $this->query($query);
        return $succes;
    }

    public function update($data, $where) {
        $query = "UPDATE " . $this->tableName . " ";
        $set = "SET ";
        $column = "";
        foreach ($data as $key => $value) {
            $column = $key . "='" . $value . "', ";
        }
        $set .= $column;
        $set = substr($set, 0, -2);
        $set .= " ";
        $query .= $set . $where;
//        sout($query);
        $succes = $this->query($query);
        return $succes;
    }

    public function getAllAsArray() {
        $query = "SELECT * FROM " . $this->getTableName();
        $res = $this->query($query);
        $result = array();
        foreach ($res as $value) {
            $result[] = $value;
        }
        return $result;
    }

    public function truncate() {
        $query = "TRUNCATE " . $this->getTableName();
        $this->query($query);
    }

    function getDsn() {
        return $this->dsn;
    }

    function getDbname() {
        return $this->dbname;
    }

    function getHost() {
        return $this->host;
    }

    function getCharset() {
        return $this->charset;
    }

    function getUser() {
        return $this->user;
    }

    function getPassword() {
        return $this->password;
    }

    function getDbh() {
        return $this->dbh;
    }

    function getData() {
        return $this->data;
    }

    function getTableName() {
        return $this->tableName;
    }

    function setDsn($dsn) {
        $this->dsn = $dsn;
    }

    function setDbname($dbname) {
        $this->dbname = $dbname;
    }

    function setHost($host) {
        $this->host = $host;
    }

    function setCharset($charset) {
        $this->charset = $charset;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setDbh($dbh) {
        $this->dbh = $dbh;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setTableName($tableName) {
        $this->tableName = $tableName;
    }

}
