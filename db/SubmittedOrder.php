<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace db;

/**
 * Description of SubmittedOrder
 *
 * @author tamas
 */
class SubmittedOrder extends Database {

    public static function instance() {
        static $inst = null;
        if ($inst === null) {
            $inst = new SubmittedOrder();
        }
        return $inst;
    }

    public function __construct() {
        parent::__construct();
        $this->data = array("id", "order_innerId");
        $this->tableName = "submitted_order";
    }

}
