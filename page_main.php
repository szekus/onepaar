<?php
if (isset($_POST["sr1_username"])) {
    file_put_contents(".settings", serialize($_POST));
    ?>
    <div class="alert alert-success" role="alert">Beállítások mentve</div>
    <?php
}

$settings = unserialize(file_get_contents(".settings"));
?>


<div class="panel">
    <h1>Fő beállítások</h1>

    <form method="POST" action="index.php">
        <!--h3>Adatforrás</h3>
        <div class="form-group">
            <label for="exampleInputEmail1">XML forrás cím</label>
            <input type="text" class="form-control" name="xml_host" placeholder="http://www.compmarket.hu/webshop/export.php?user=goldchip2&pass=gold_shop&fields=cikkszam,gy_cikkszam,kategoria,kategoria_id,gyarto,gyarto_id,nev,tipus,leiras,leiras2,kep_link,link,ar,garido,keszlet,vonalkod,suly,csoport,beerkezes,specifikacio&type=order&format=xml" value="<?php echo $settings["xml_host"]; ?>">
        </div-->

        <h3>ShopRenter Adatok</h3>

        <div class="form-group">
            <label for="exampleInputEmail1">ShopRenter felhasználói név</label>
            <input type="text" class="form-control" name="sr1_username" placeholder="shopuser" value="<?php echo $settings["sr1_username"]; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">ShopRenter API kulcs</label>
            <input type="text" class="form-control" name="sr1_apikey"  placeholder="kodsor" value="<?php echo $settings["sr1_apikey"]; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">ShopRenter API URL</label>
            <input type="text" class="form-control" name="sr1_host"  placeholder="http://shopnamne.api.shoprenter.hu" value="<?php echo $settings["sr1_host"]; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">OnePaar felhasználói név</label>
            <input type="text" class="form-control" name="bg_username" placeholder="bg_username" value="<?php echo $settings["bg_username"]; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">OnePaar password</label>
            <input type="text" class="form-control" name="bg_password"  placeholder="jelszo" value="<?php echo $settings["bg_password"]; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">OnePaar API URL</label>
            <input type="text" class="form-control" name="bg_host"  placeholder="https://v1.api.brandsgateway.com" value="<?php echo $settings["bg_host"]; ?>">
        </div>
        <button type="submit" class="btn btn-primary">Mentés</button>
    </form>

</div>
