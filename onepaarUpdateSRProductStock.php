<?php

echo '<pre>';
//
//
require_once('loader.php');
require_once('helpers\shoprenter.php');
require_once('helpers\utils.php');
require_once('settings.php');

define("LIMIT", -1);
$counter = 0;

$bgProductStockInfo = \db\BgProductStockInfo::instance();

$infos = $bgProductStockInfo->getAllAsArray();


foreach ($infos as $productBG) {
    $counter++;
    $productSR = resources\Product::create();
    $sku = $productBG["sku"];
    $productSR->sku = $sku;
    $id = $productSR->getProductIdBySku($sku);
    $productSR->id = $id;
    $stock = $productBG["stock_quantity"];

    if ($productBG["in_stock"] == "true") {
        if ($stock === "unlimited") {
            $stock = 100;
            $productSR->subtractStock = 0;
        }
    } else {
        $productSR->status = 0;
    }
    $productSR->stock1 = $stock;
    
    $productSR->update(true, "POST");

    if ($counter == LIMIT) {
        break;
    }
}