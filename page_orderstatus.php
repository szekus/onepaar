<?php
ini_set('display_errors', '1');

$orderStatusResource = \resources\OrderStatus::create();
$select = $orderStatusResource->createOrderStausSelectArray();

$submittedStatus = \db\StatusToSubmitted::instance();
$selectesStatusId = $submittedStatus->getSelectedStatusId();

$selectesStatusName = "";

foreach ($select as $value) {
    if ($value["id"] == $selectesStatusId) {
        $selectesStatusName = $value["name"];
    }
}


if (isset($_POST) && count($_POST) > 0) {
//    sout($_POST);

    $data = array("status_id" => $_POST["select_status"]);
//    sout($data);
    ($submittedStatus->update($data, "WHERE id = 1"));
    echo "<meta http-equiv='refresh' content='0'>";
}
?>

<?php
?>

<div class="panel">
    <h1>Retailer order sync status</h1>
    <div class="jumbotron">
        <p>
            All orders with the choosen status will be sent to the retailer to ship the order
        </p>
    </div>

    <br>
    <form method="post" action="index.php?page=orderstatus" enctype="multipart/form-data" >
        <table class="table">
            <tr>
                <th> SR státusz: <?php echo $selectesStatusName; ?></th>
            </tr>
            <select name="select_status" class="dropdown" id="select_status">
                <?php
                foreach ($select as $value) {
                    $id = $value["id"];
                    $name = $value["name"];
                    $setSelected = "selected";

                    if ($id == $selectesStatusId) {
                        echo "<option selected value='$id'>$name</option>";
                    } else {
                        echo "<option value='$id'>$name</option>";
                    }
                }
                ?>
            </select>

        </table>
        <button type="submit" class="btn btn-primary">Mentés</button>
    </form>
</div>
