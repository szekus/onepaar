<?php
require_once('loader.php');
require_once('helpers\shoprenter.php');
require_once('helpers\utils.php');
require_once('settings.php');



// Kategories
// $endpointPath = API_ENDPOINT_CATEGORY."?page=0&limit=200";
// $endpointPath = API_ENDPOINT_CUSTOMER_GROUP."?page=0&limit=200";

/*$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD04';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xMQ==';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xMg==';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xMw==';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNA==';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNQ==';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNg==';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xNw==';
$elist[] = '/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD0xOA==';
*/
$elist[] = '/addresses/YWRkcmVzcy1hZGRyZXNzX2lkPTI=';

foreach($elist as $endpointPath) {
  // $endpointPath = "/customerGroups/Y3VzdG9tZXJHcm91cC1jdXN0b21lcl9ncm91cF9pZD04";
  echo $endpointPath.'<br>';
  do {
    $result = querySRApi($endpointPath, [], 'GET');
    pre_print($result);
    $batchRequest['requests'] = [];
    foreach($result['items'] as $item) {
      pre_print($item);
      $batchRequest['requests'][] = [
          'method' => 'GET',
          'uri' => $item['href']
      ];
    }

    if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
       pre_print(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
    }
  } while (!is_null($result['next']));
}

exit;

// $endpointPath = API_ENDPOINT_PRODUCT_CLASS."?page=0&limit=200";
$endpointPath = '/urlAliases?productId=cHJvZHVjdC1wcm9kdWN0X2lkPTMyMA==&page=0&limit=25';
do {
  $result = querySRApi($endpointPath, [], 'GET');
  $batchRequest['requests'] = [];
  foreach($result['items'] as $item) {
    print_r($item);
    $batchRequest['requests'][] = [
        'method' => 'GET',
        'uri' => $item['href']
    ];

  }

  if (is_array($batchRequest) && count($batchRequest['requests'])>0) {
     var_dump(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
  }
} while (!is_null($result['next']));
