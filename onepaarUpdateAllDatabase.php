<?php

require_once('loader.php');
require_once('helpers\shoprenter.php');
require_once('helpers\utils.php');
require_once('settings.php');


//$orderStatusResource = \resources\OrderStatus::create();
//$orderStatusResource->deleteFromDB();
//$ordersStatuses = $orderStatusResource->getAll();
//$orderStatusResource->setData($ordersStatuses);
//$orderStatusResource->insertToDB($ordersStatuses);
//
//
//$orderStatusDescriptionResource = \resources\OrderStatusDescription::create();
//$orderStatusDescriptionResource->deleteFromDB();
//$ordersStatusDescriptions = $orderStatusDescriptionResource->getAll();
//$orderStatusResource->setData($ordersStatusDescriptions);
//$orderStatusDescriptionResource->insertToDB($ordersStatusDescriptions);
//
//$orderResource = \resources\Order::create();
//$orderResource->deleteFromDB();
//$orders = $orderResource->getAll();
//$orderResource->setData($orders);
//$orderResource->insertToDB($orders);
//
$productResource = \resources\Product::create();
//$productResource->deleteFromDB();
//$products = $productResource->getAll();
//$productResource->setData($products);
//$productResource->insertToDB($products);


$sr_products = $productResource->getAllFromDB();
$storageInfo = array();
foreach ($sr_products as $sr_product) {
    $sku = $sr_product->sku;

    $bgProduct = new \bgresource\BGProduct();
    $bgProdInfo = $bgProduct->getBySkuOrVariant($sku);

    if ($bgProdInfo != null) {
        $storageInfo[] = $bgProdInfo;
    }
}

$bgProductStockInfo = \db\BgProductStockInfo::instance();
$bgProductStockInfo->truncate();
foreach ($storageInfo as $info) {
    $bgProductStockInfo = \db\BgProductStockInfo::instance();
    $bgProductStockInfo->insert($info);
}
