<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductTag
 *
 * @author szekus
 */
class ProductTag extends Resource {

    private $product;
    private $language;
    private $tags;

    function getProduct() {
        return $this->product;
    }

    function getLanguage() {
        return $this->language;
    }

    function getTags() {
        return $this->tags;
    }

    function setProduct($product) {
        $this->product = $product;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setTags($tags) {
        $this->tags = $tags;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productTags";
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

    

}
