<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ManufacturerDescription
 *
 * @author szekus
 */
class ManufacturerDescription extends Resource {

    private $description;
    private $customTitle;
    private $metaDescription;
    private $metaKeywords;
    private $manufacturer;
    private $language;

    function getDescription() {
        return $this->description;
    }

    function getCustomTitle() {
        return $this->customTitle;
    }

    function getMetaDescription() {
        return $this->metaDescription;
    }

    function getMetaKeywords() {
        return $this->metaKeywords;
    }

    function getManufacturer() {
        return $this->manufacturer;
    }

    function getLanguage() {
        return $this->language;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setCustomTitle($customTitle) {
        $this->customTitle = $customTitle;
    }

    function setMetaDescription($metaDescription) {
        $this->metaDescription = $metaDescription;
    }

    function setMetaKeywords($metaKeywords) {
        $this->metaKeywords = $metaKeywords;
    }

    function setManufacturer($manufacturer) {
        $this->manufacturer = $manufacturer;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    public function __construct() {
        parent::__construct();
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

}
