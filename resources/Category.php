<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Category
 *
 * @author tamas
 */
class Category extends Resource {

    private $picture;
    private $sortOrder;
    private $status;
    private $productStatus;
    private $parentCategory;
    private $centralCategory;
    private $customerGroups;
    private $categoryCustomerGroupRelations;

    public function __construct() {
        parent::__construct();
    }

    function getPicture() {
        return $this->picture;
    }

    function getSortOrder() {
        return $this->sortOrder;
    }

    function getStatus() {
        return $this->status;
    }

    function getProductStatus() {
        return $this->productStatus;
    }

    function getParentCategory() {
        return $this->parentCategory;
    }

    function getCentralCategory() {
        return $this->centralCategory;
    }

    function getCustomerGroups() {
        return $this->customerGroups;
    }

    function getCategoryCustomerGroupRelations() {
        return $this->categoryCustomerGroupRelations;
    }

    function setPicture($picture) {
        $this->picture = $picture;
    }

    function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setProductStatus($productStatus) {
        $this->productStatus = $productStatus;
    }

    function setParentCategory($parentCategory) {
        $this->parentCategory = $parentCategory;
    }

    function setCentralCategory($centralCategory) {
        $this->centralCategory = $centralCategory;
    }

    function setCustomerGroups($customerGroups) {
        $this->customerGroups = $customerGroups;
    }

    function setCategoryCustomerGroupRelations($categoryCustomerGroupRelations) {
        $this->categoryCustomerGroupRelations = $categoryCustomerGroupRelations;
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

}
