<?php

require_once 'response.php';

/**
 * Api Call
 *
 * @author Kántor András
 * @since 2013.02.22. 13:43
 */
class ApiCall {

    /**
     * @var string
     */
    protected $username = '';

    /**
     * @var string
     */
    protected $apiKey = '';

    /**
     * @var string
     */
    protected $response;

    /**
     * @var string
     */
    protected $format = 'json';

    /**
     * @param string $username
     * @param string $apiKey
     */
    public function __construct($username, $apiKey) {
        $this->username = $username;
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @throws Exception
     * @return Response
     */
    public function execute($method, $url, array $data = array()) {
        $this->ensure($this->username != '', 'Username cannot be empty!');
        $this->ensure($this->apiKey != '', 'Api key cannot be empty!');

        $curlHandle = curl_init();
        $this->setUrl($curlHandle, $url);
        $this->setAuth($curlHandle);
        $this->setOptions($curlHandle);
        $retval = true;
        error_reporting(E_WARNING);
        switch ($method) {
            case 'GET':
                $retval = $this->executeGet($curlHandle);
                break;
            case 'POST':
                $retval = $this->executePost($curlHandle, $data);
                break;
            case 'PUT':
                $retval = $this->executePut($curlHandle, $data);
                break;
            case 'DELETE':
                $retval = $this->executeDelete($curlHandle);
                break;
            default:
                throw new Exception('Invalid HTTP METHOD');
        }
        error_reporting(E_ALL);

        if ($retval)
            return $this->response;
        return false;
    }

    /**
     * @return string
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getFormat() {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format) {
        $this->format = $format;
    }

    /**
     * @param $curlHandle
     * @param string $url
     */
    protected function setUrl($curlHandle, $url) {
        curl_setopt($curlHandle, CURLOPT_URL, $url);
    }

    /**
     * @param $curlHandle
     */
    protected function setAuth($curlHandle) {
        curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->apiKey);
    }

    /**
     * @param $curlHandle
     */
    protected function setOptions($curlHandle) {
        curl_setopt($curlHandle, CURLOPT_HEADER, 1);
        curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curlHandle, CURLOPT_MAXREDIRS, 5);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array("Content-type: multiform/post-data"));
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array("Expect:"));
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array("Accept: application/" . $this->format));
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 600);
    }

    /**
     * @param $curlHandle
     */
    protected function executeGet($curlHandle) {
        return $this->doExecute($curlHandle);
    }

    /**
     * @param $curlHandle
     * @param array $data
     */
    protected function executePost($curlHandle, array $data) {
        $query = http_build_query(array('data' => $data));
        curl_setopt($curlHandle, CURLOPT_POST, true);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $query);

        return $this->doExecute($curlHandle);
    }

    /**
     * @param $curlHandle
     * @param array $data
     */
    protected function executePut($curlHandle, array $data) {
        $query = http_build_query(array('data' => $data));
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $query);

        return $this->doExecute($curlHandle);
    }

    /**
     * @param $curlHandle
     */
    protected function executeDelete($curlHandle) {
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, 'DELETE');

        return $this->doExecute($curlHandle);
    }

    /**
     * @param $curlHandle
     * @return string
     */
    protected function doExecute($curlHandle) {
        ob_start();
        curl_exec($curlHandle);
        $content = ob_get_contents();
        ob_end_clean();
        if (@sizeof($content) < 1)
            return FALSE;
        list($headers, $responseBody) = explode("\r\n\r\n", $content, 2);
        $statusCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        $contentType = curl_getinfo($curlHandle, CURLINFO_CONTENT_TYPE);
        
        preg_match("!\r\n(?:Location|URI): *(.*?) *\r\n!", $headers, $matches);
        $location = isset($matches[1]) ? $matches[1] : '';
        $this->response = new Response($statusCode, $contentType, $location, $responseBody);

        curl_close($curlHandle);
        return true;
    }

    /**
     * @param bool $bool
     * @param string $errorMessage
     * @throws Exception
     */
    protected function ensure($bool, $errorMessage) {
        if (!$bool) {
            throw new Exception($errorMessage);
        }
    }

}
