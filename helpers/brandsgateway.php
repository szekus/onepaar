<?php

//DEFINE('QUERY_DEBUG', false);

function queryBGApi($path, $arr = [], $method = 'GET', $retType = "responseBody", $debug = false) {
    $apiCall = new ApiCall(BG_USERNAME, BG_APIKEY);
    $apiCall->setFormat('json');
    $response = $apiCall->execute($method, BG_APIURL . $path, $arr);
    if ($debug || QUERY_DEBUG)
        print_r($response);
    if ($response->getStatusCode() == 409) {
        $response = $apiCall->execute('DELETE', BG_APIURL . $path . $response->getParsedResponseBody()["id"], []);
        $response = $apiCall->execute($method, BG_APIURL . $path, $arr);
    }
    if ($debug || QUERY_DEBUG)
        echo BG_APIURL . $path . '<br>';
    if ($retType == "responseBody")
        return $response->getParsedResponseBody();
    if ($retType == "statusCode")
        return $response->getStatusCode();
}
