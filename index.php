<?php
session_start();
require_once('loader.php');
require_once('helpers\shoprenter.php');
require_once('helpers\utils.php');
require_once('settings.php');
$page = "main";
if (isset($_GET["page"]))
    $page = $_GET["page"];



if (isset($_POST["site_username"])) {
    if ($_POST["site_username"] == "onepaaradmin" && $_POST["site_password"] == "onepaaradmin12")
        $_SESSION["username"] = "onepaaradmin";
}

if (isset($_GET["logout"]))
    unset($_SESSION["username"]);

if (!isset($_SESSION["username"]) || $_SESSION["username"] != 'onepaaradmin')
    $page = "login";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OnePaar Adminisztrációs felület</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="container-fluid">

        <nav class="navbar navbar-default">
            <div>
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">ShopSync</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li <?php if ($page == "main") { ?>class="active"<?php } ?>><a href="?page=main">Főoldal</a></li>
                        <li <?php if ($page == "orderstatus") { ?>class="active"<?php } ?>><a href="?page=orderstatus">Rendelés státuszok</a></li>
                        <?php if (isset($_SESSION["username"])) { ?><li><a href="?logout=true">Kilépés</a></li><?php } ?>
                    </ul>


                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

    <center>
        <?php
        switch ($page) {
            case "main":
                require_once "page_main.php";
                break;

            case "orderstatus":
                require_once "page_orderstatus.php";
                break;
            case "login":
                require_once "page_login.php";
                break;
        }
        ?>
    </center>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
