<?php

namespace bgresource;

class BGOrder extends BGResource {

    protected $shipping;
    protected $line_items;
    protected $srInnerId;

    public function __construct() {
        parent::__construct();

        $this->apiEndpoint = "/orders";
    }

    public function setByArray($array) {
        foreach ($array as $key => $value) {
            if ($key == "shipping") {
                $this->shipping = new Shipping();
                $this->shipping->setAttributes($value);
                $this->shipping = $this->shipping->getAsArray();
            }
            if ($key == "line_items") {
                foreach ($value as $val) {
                    $lineItem = new LineItem();
                    $lineItem->setAttributes($val);
                    $this->line_items[] = $lineItem->getAsArray();
                }
            }
        }
    }

    public function setToOrderArray() {
        $order = $this->getAsArray();
        $result = array();

        foreach ($order as $key => $value) {
            if ($key == "shipping") {
                $result["shipping"] = $value;
            }
            if ($key == "line_items") {
                $result["line_items"] = $value;
            }
        }
        
        return $result;
    }

    public function getAsArray() {
//        $result = array();
//
//
//        $shipping = array();
//        foreach ($this->shipping as $value) {
//            $shipping[] = $value->getAsArray();
//            
//        }
//
//        $line_items = array();
//        foreach ($this->line_items as $value) {
//            $line_item = array();
////            $line_item = $value;
//            $line_item[] = $value->getAsArray();
//            $line_items[] = $line_item;
//        }
//
//        $result["shipping"] = $shipping;
//        $result["line_items"] = $line_items;
//        return $result;

        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

    public function insert($debug = false, $type = "POST") {
        $array = $this->setToOrderArray();
        $result = queryBGApi($this->apiEndpoint, $array, $type, "responseBody", $debug);
        return $result;
    }

}

class Attribute {

    public function setAttributes($array) {
        if (is_array($array) || is_object($array)) {
            foreach ($array as $key => $value) {
                $this->{$key} = $value;
            }
        } else {
//            sout("NOT AN ARRAY: ");
//            sout($array);
        }
    }

    public function getAsArray() {
        return (get_object_vars($this));
    }

}

class Shipping extends Attribute {

    public $first_name;
    public $last_name;
    public $address_1;
    public $address_2;
    public $city;
    public $state;
    public $postcode;
    public $country;

    public function __construct() {
        ;
    }

}

class LineItem extends Attribute {

    public $product_id;
    public $variation_id;
    public $quantity;

    public function __construct() {
        ;
    }

}
