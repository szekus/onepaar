<?php

namespace bgresource;

require_once('helpers\brandsgateway.php');
require_once('helpers\utils.php');
require_once('settings.php');



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BGResource
 *
 * @author tamas
 */
class BGResource {

    protected $id;
    protected $apiEndpoint;
    protected $per_page;
    protected $page;

    public function __construct() {
        ;
    }

    function getApiEndpoint() {
        return $this->apiEndpoint;
    }

    function setApiEndpoint($apiEndpoint) {
        $this->apiEndpoint = $apiEndpoint;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getPer_page() {
        return $this->per_page;
    }

    function getPage() {
        return $this->page;
    }

    function setPer_page($per_page) {
        $this->per_page = $per_page;
    }

    function setPage($page) {
        $this->page = $page;
    }

    public function getAll() {
        
    }

    public function getAsArray() {
        return (get_object_vars($this));
    }

}
