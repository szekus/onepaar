<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace bgresource;

/**
 * Description of BGProductCSV
 *
 * @author tamas
 */
class BGProductCSV {

    private $csv;
    private $csvArray;
    private $bgProductArray;

    public function __construct($bgProductArray) {
        $this->bgProductArray = $bgProductArray;
    }

    function createCSV() {
        foreach ($this->bgProductArray as $bgProduct) {
            $this->csv = array();
            $this->csv["sku"] = $bgProduct->getSku();
            $this->csv["stock_quantity"] = $bgProduct->getStock_quantity();
            $this->csv["in_stock"] = $bgProduct->getIn_stock();
            $this->csvArray[] = $this->csv;

            $variations = $bgProduct->getVariations();
            if ($variations != null) {
                foreach ($variations as $variation) {
                    $this->csv = array();
                    $this->csv["sku"] = $variation->getSku();
                    $this->csv["stock_quantity"] = $variation->getStock_quantity();
                    $this->csv["in_stock"] = $variation->getIn_stock();
                    $this->csvArray[] = $this->csv;
                }
            }
        }

        $headers = array("sku", "stock_quantity", "in_stock");
        $fp = fopen('data/bgProducts.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($this->csvArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

}
