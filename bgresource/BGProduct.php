<?php

namespace bgresource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BGProduct
 *
 * @author tamas
 */
class BGProduct extends BGResource {

    public function __construct() {
        parent::__construct();
        $this->per_page = 100;
        $this->page = 58;
        $this->apiEndpoint = "/products";
    }

    public function getLastPage($isTest = false) {
        $max = 56;

        if ($isTest) {
            return 1;
        } else {
            $bool = true;
            while ($bool) {
                $res = queryBGApi("/products?per_page=" . $this->per_page . "&page=" . $max, [], "GET", "responseBody", false);
                if (!empty($res)) {
                    $max++;
                } else {
                    $bool = false;
                }
            };
            return $max;
        }
    }

    public function getAll() {
        $result = [];
        $endpointUrl = $this->apiEndpoint . "?per_page=" . $this->per_page . "&page=";
        $lastPage = $this->getLastPage(true);
        for ($i = 1; $i <= $lastPage; $i++) {
            $res = queryBGApi($endpointUrl . $i, [], "GET", "responseBody", false);
//            array_push($result, $res);
            for ($j = 0; $j < count($res); $j++) {
                $result[] = $res[$j];
            }
            $result[] = $res;
//        sout($result);
        }

        return $result;
    }

    public function getBySku($sku) {
        $url = $this->apiEndpoint . "?sku=" . $sku;
//        sout($url);
        $result = queryBGApi($url, [], "GET", "responseBody", false);
        if ($result != null) {
            $this->setAttributesByArray($result[0]);
        }

        return $this;
    }

    public function getBySkuOrVariant($sku) {
        $result = array();
        $skuVariants = explode("-", $sku);
        $originalSku = $skuVariants[0];
        $variantsNumber = 0;
        $bgProduct = new BGProduct();
        if (count($skuVariants) > 1) {
//            if (!is_numeric($skuVariants[1])) {
//                sout($skuVariants[1]);
//                sout($sku);
//            }
            $variantsNumber = $skuVariants[1] - 1;

            $bgProduct = $bgProduct->getBySku($originalSku);

            if ($bgProduct->getSku() != null) {
                $variants = $bgProduct->getVariations();
//            sout(;
//            die();
                if ($variants != null && count($variants) >= $skuVariants[1]) {
//                var_dump($variantsNumber);
                    $variant = $variants[$variantsNumber];
                    $result["sku"] = $sku;
                    if ($variant->getIn_stock()) {
                        $result["in_stock"] = "true";
                    } else {
                        $result["in_stock"] = "false";
                    }
//                    sout(!is_numeric($variant->getStock_quantity()));
                    if ($variant->getStock_quantity() == null && !is_numeric($variant->getStock_quantity())) {
                        $result["stock_quantity"] = "unlimited";
//                        sout("true");
                    } else {
                        $result["stock_quantity"] = $variant->getStock_quantity();
//                        sout("false");
                    }

                    return $result;
                }
            }
        } else {
            $bgProduct = $bgProduct->getBySku($sku);
            if ($bgProduct->getSku() != null) {
                $result["sku"] = $sku;
                if ($bgProduct->getIn_stock() == "true") {
                    $result["in_stock"] = "true";
                } else {
                    $result["in_stock"] = "false";
                }
                if ($bgProduct->getStock_quantity() == null || !is_numeric($bgProduct->getStock_quantity())) {
                    $result["stock_quantity"] = "unlimited";
                } else {
                    $result["stock_quantity"] = $bgProduct->getStock_quantity();
                }
//            $result["in_stock"] = $bgProduct->getIn_stock();
//            $result["stock_quantity"] = $bgProduct->getStock_quantity();
                return $result;
            }
        }
//        $result["no_product_sku"] = $sku;
//        return $result;
    }

    public function setAttributesByArray($array) {
//        sout($array);
        foreach ($array as $key => $value) {
//            sout($key);
            switch ($key) {
                case "categories":
                    foreach ($value as $categoryValue) {

                        $categories = new Categories();
                        $categories->setAttributes($categoryValue);
                        $this->categories[] = $categories;
                    }
                    break;
                case "images":
                    foreach ($value as $imageValue) {
                        $images = new Images();
                        $images->setAttributes($imageValue);
                        $this->images[] = $images;
                    }
                    break;
                case "attributes":
                    foreach ($value as $attributesValue) {
                        $attributes = new Attributes();
                        $images->setAttributes($attributesValue);
                        $this->images[] = $images;
                    }
                    break;
                case "variations":
                    foreach ($value as $variationValue) {
                        $variations = new Variations();
//                        sout($variationValue);
                        $variations->setAttributes($variationValue);
                        $this->variations[] = $variations;
                    }
                    break;
                case "brands":
                    foreach ($value as $brandsValue) {
                        $brands = new Variations();
                        $brands->setAttributes($brandsValue);
                        $this->brands[] = $brands;
                    }
                    break;
                default :
                    $this->{$key} = $value;
                    break;
            }
        }
    }

//    protected $bgProductList;
    protected $name;
    protected $permalink;
    protected $date_created;
    protected $date_modified;
    protected $description;
    protected $short_description;
    protected $sku;
    protected $price;
    protected $regular_price;
    protected $sale_price;
    protected $price_html;
    protected $stock_quantity;
    protected $in_stock;
    protected $parent_id;
    protected $categories;
    protected $images;
    protected $attributes;
    protected $variations;
    protected $brands;

    function getBgProductList() {
        return $this->bgProductList;
    }

    function getSku() {
        return $this->sku;
    }

    function setBgProductList($bgProductList) {
        $this->bgProductList = $bgProductList;
    }

    function setSku($sku) {
        $this->sku = $sku;
    }

    function getName() {
        return $this->name;
    }

    function getPermalink() {
        return $this->permalink;
    }

    function getDate_created() {
        return $this->date_created;
    }

    function getDate_modified() {
        return $this->date_modified;
    }

    function getDescription() {
        return $this->description;
    }

    function getShort_description() {
        return $this->short_description;
    }

    function getPrice() {
        return $this->price;
    }

    function getRegular_price() {
        return $this->regular_price;
    }

    function getSale_price() {
        return $this->sale_price;
    }

    function getPrice_html() {
        return $this->price_html;
    }

    public function getStock_quantity() {
        return $this->stock_quantity;
    }

    function getIn_stock() {
        return $this->in_stock;
    }

    function getParent_id() {
        return $this->parent_id;
    }

    function getCategories() {
        return $this->categories;
    }

    function getImages() {
        return $this->images;
    }

    function getAttributes() {
        return $this->attributes;
    }

    function getVariations() {
        return $this->variations;
    }

    function getBrands() {
        return $this->brands;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPermalink($permalink) {
        $this->permalink = $permalink;
    }

    function setDate_created($date_created) {
        $this->date_created = $date_created;
    }

    function setDate_modified($date_modified) {
        $this->date_modified = $date_modified;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setShort_description($short_description) {
        $this->short_description = $short_description;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setRegular_price($regular_price) {
        $this->regular_price = $regular_price;
    }

    function setSale_price($sale_price) {
        $this->sale_price = $sale_price;
    }

    function setPrice_html($price_html) {
        $this->price_html = $price_html;
    }

    function setStock_quantity($stock_quantity) {
        $this->stock_quantity = $stock_quantity;
    }

    function setIn_stock($in_stock) {
        $this->in_stock = $in_stock;
    }

    function setParent_id($parent_id) {
        $this->parent_id = $parent_id;
    }

    function setCategories($categories) {
        $this->categories = $categories;
    }

    function setImages($images) {
        $this->images = $images;
    }

    function setAttributes($attributes) {
        $this->attributes = $attributes;
    }

    function setVariations($variations) {
        $this->variations = $variations;
    }

    function setBrands($brands) {
        $this->brands = $brands;
    }

}

class Categories extends ProductAttribute {

    protected $id;
    protected $name;

    public function __construct() {
        
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

}

class Images extends ProductAttribute {

    protected $id;
    protected $date_created;
    protected $date_modified;
    protected $src;
    protected $name;

    public function __construct() {
        ;
    }

    function getId() {
        return $this->id;
    }

    function getDate_created() {
        return $this->date_created;
    }

    function getDate_modified() {
        return $this->date_modified;
    }

    function getSrc() {
        return $this->src;
    }

    function getName() {
        return $this->name;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDate_created($date_created) {
        $this->date_created = $date_created;
    }

    function setDate_modified($date_modified) {
        $this->date_modified = $date_modified;
    }

    function setSrc($src) {
        $this->src = $src;
    }

    function setName($name) {
        $this->name = $name;
    }

}

class Image extends Images {
    
}

class Attributes extends ProductAttribute {

    protected $id;
    protected $name;
    protected $visible;
    protected $variation;
    protected $options;

    public function __construct() {
        ;
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getVisible() {
        return $this->visible;
    }

    function getVariation() {
        return $this->variation;
    }

    function getOptions() {
        return $this->options;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setVisible($visible) {
        $this->visible = $visible;
    }

    function setVariation($variation) {
        $this->variation = $variation;
    }

    function setOptions($options) {
        $this->options = $options;
    }

}

class Variations extends ProductAttribute {

    public function setAttributes($array) {
        foreach ($array as $key => $value) {

            switch ($key) {
//                case "categories":
//                    foreach ($value as $categoryValue) {
//                        $categories = new Categories();
//                        $categories->setAttributes($categoryValue);
//                        $this->categories[] = $categories;
//                    }
//
//                    break;
                case "image":
                    foreach ($value as $imageValue) {
                        $image = new Image();
                        $image->setAttributes($imageValue);
                        $this->image[] = $image;
                    }
                    break;
                case "attributes":
                    foreach ($value as $attributesValue) {
                        $attributes = new Attributes();
                        $attributes->setAttributes($attributesValue);
                        $this->attributes[] = $attributes;
                    }
                    break;

                    break;
//                case "brands":
//                    foreach ($value as $brandsValue) {
//                        $brands = new Variations();
//                        $brands->setAttributes($brandsValue);
//                        $this->variations[] = $brands;
//                    }
//                    break;
                default :
                    $this->{$key} = $value;
                    break;
            }
        }
    }

    protected $id;
    protected $date_created;
    protected $date_modified;
    protected $permalink;
    protected $sku;
    protected $price;
    protected $in_stock;
    protected $image;
    protected $attributes;
    protected $regular_price;
    protected $stock_quantity;

    function getRegular_price() {
        return $this->regular_price;
    }

    public function getStock_quantity() {
        return $this->stock_quantity;
    }

    function setRegular_price($regular_price) {
        $this->regular_price = $regular_price;
    }

    function setStock_quantity($stock_quantity) {
        $this->stock_quantity = $stock_quantity;
    }

    function getId() {
        return $this->id;
    }

    function getDate_created() {
        return $this->date_created;
    }

    function getDate_modified() {
        return $this->date_modified;
    }

    function getPermalink() {
        return $this->permalink;
    }

    public function getSku() {
        return $this->sku;
    }

    function getPrice() {
        return $this->price;
    }

    public function getIn_stock() {
        return $this->in_stock;
    }

    function getImage() {
        return $this->image;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDate_created($date_created) {
        $this->date_created = $date_created;
    }

    function setDate_modified($date_modified) {
        $this->date_modified = $date_modified;
    }

    function setPermalink($permalink) {
        $this->permalink = $permalink;
    }

    function setSku($sku) {
        $this->sku = $sku;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setIn_stock($in_stock) {
        $this->in_stock = $in_stock;
    }

    function setImage($image) {
        $this->image = $image;
    }

}

class Brands extends ProductAttribute {

    protected $id;
    protected $name;

    public function __construct() {
        ;
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

}

class ProductAttribute {

    public function setAttributes($array) {
        if (is_array($array) || is_object($array)) {
            foreach ($array as $key => $value) {
                $this->{$key} = $value;
            }
        } else {
//            sout("NOT AN ARRAY: ");
//            sout($array);
        }
    }

}
